const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

app.use(express.json()); // Pasa a JSON todo lo que se deja en el body

app.listen(port);
console.log("API escuchando en el puerto de AFE-plus beep bepp " + port);

app.get('/apitechu/v1/hello',
  function (req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Hola desde API TechU"});
  }

)

app.get('/apitechu/v1/users',
  function (req, res) {
    console.log("GET /apitechu/v1/users");
    var users = require('./usuarios.json'); // ./ indica que es el directorio donde estamos
    res.send(users);

}

)
// POST
app.post('/apitechu/v1/users',
  function (req, res) {
    console.log("POST /apitechu/v1/hello");

    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    var newUser = {
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
    }

    var users = require('./usuarios.json');
    users.push(newUser); // empotra el nuevo usuario de la cabecera en el usuarios.json
    writeUserDatatofile(users); // funcion externalizada
    console.log("Usuario añadido con éxito");
    res.send({"msg" : "Usuario añadido con exito"})

  }
)
//
// DELETE Ejemplo
//
app.delete("/apitechu/v1/users/:id",
  function(req, res) {
    console.log ("DELETE /apitechu/v1/users/:id");
    console.log ("id es " + req.params.id);

    var users = require('./usuarios.json');

//    users.splice(req.params.id -1, 1);  // Parametro 1: elemento del array nº
                                        // Parametro 2: numero de elemenentos a borrar
    console.log("Usuario borrado");
    writeUserDatatofile(users);
    res.send({"msg" : "Usuario borrado con exito-AFE"})
  }
)

//
// DELETE con FOR tipo 1. Normal.
//
app.delete("/apitechu/v1/users/For1/:id",
  function(req, res) {
    console.log ("DELETE /apitechu/v1/users/For1:id");
    console.log ("id es " + req.params.id);

    var users = require('./usuarios.json'); // Carga el objeto users con el fichero
// Loop
        encontrado = false;
        var i;
        for (i = 0; i < users.length; i++) {
          console.log('---------------------');
          console.log('En bucle (indice):            ' + i);
          console.log('longitud del fichero (hasta): ' + users.length);
          console.log('Id a buscar: ' + req.params.id);
          console.log('Usuario del fichero: ' + users[i].id);
          if (users[i].id == req.params.id) {
              encontrado = true;
              console.log('****** localizado ******');
              console.log('Quieres borrar el Id :' + users[i].id);
              console.log('Y en el fichero es   :' + req.params.id);
              console.log('Ocurrencia numero    :' + i);
              // Quitamos del array el que nos interesa
              users.splice(i, 1);                // Parametro 1: ocurrencia del array nº
                                                 // Parametro 2: numero de elementos a cortar
              break; // encontrado y purgado del array.
          }
        };
// Fin loop
//
// Array users listo para ser cargado sin el registro seleccionado.
    if (encontrado) {
       console.log("Array users lista para ser cargada: usuario borrado");
       writeUserDatatofile(users);
       res.send({"msg" : "Usuario borrado con For tipo-1"})
     }
    else{
       console.log("Usuario no encontrado");
       res.send({"msg" : "Usuario NO borrado"})
       }
  }
)

//
// DELETE con FOR tipo 2. Array.forEach.
//
app.delete("/apitechu/v1/users/For2/:id",
  function(req, res) {
    console.log ("DELETE /apitechu/v1/users/For2:id");
    console.log ("id es " + req.params.id);

    var users = require('./usuarios.json'); // Carga el objeto users con el fichero

    var i;
    var valorusuario;
    encontrado = false;
// Loop
    users.forEach(function(valorusuario, i) {
          console.log("====== En el array foreach =======");
          console.log("Usuario :" + valorusuario.id);
          console.log("i       :" + i);
//          if (users[i].id == req.params.id) {
          if (valorusuario.id == req.params.id) {
            encontrado = true;
            console.log('****** localizado ******');
            console.log('Quieres borrar el Id :' + users[i].id);
            console.log('Y en el fichero es   :' + req.params.id);
            console.log('Ocurrencia numero    :' + i);
            users.splice(i, 1);
          }
        }
      );

    if (encontrado) {
       console.log("ArrayforEach users lista para ser cargada: usuario borrado");
       writeUserDatatofile(users);
       res.send({"msg" : "Usuario borrado con For tipo-2"})
     }
    else{
       console.log("Usuario no encontrado con ArrayforEach");
       res.send({"msg" : "Usuario NO borrado"})
       }
    }
  )

//
// DELETE con FOR tipo 3: array in.
//
  app.delete("/apitechu/v1/users/For3/:id",
    function(req, res) {
      console.log ("DELETE /apitechu/v1/users/For3:id");
      console.log ("id es " + req.params.id);

      var users = require('./usuarios.json'); // Carga el objeto users con el fichero

      encontrado = false;
// Loop
      for (arrayId in users) {
            console.log("====== En el arrayId in array ======");
            console.log("Usuario :" + arrayId);
            console.log("Valor usuario :" + users[arrayId].id);
            if (users[arrayId].id == req.params.id) {
              encontrado = true;
              console.log('****** localizado ******');
              console.log('Quieres borrar el Id :' + users[arrayId].id);
              console.log('Y en el fichero es   :' + req.params.id);
              users.splice(arrayId, 1);
            }
          }

      if (encontrado) {
         console.log("arrayId in array users lista para ser cargada: usuario borrado");
         writeUserDatatofile(users);
         res.send({"msg" : "Usuario borrado con For tipo-3"});
       }
      else{
         console.log("Usuario no encontrado con arrayId in array");
         res.send({"msg" : "Usuario NO borrado"})
         }
      }
    )

//
// DELETE con FOR OF tipo 4-1: array on.
//
          app.delete("/apitechu/v1/users/For4-1/:id",
            function(req, res) {

              console.log ("DELETE /apitechu/v1/users/For4-1:id");
              console.log ("id es " + req.params.id);

              var users = require('./usuarios.json'); // Carga el objeto users con el fichero

              var i = 0;
              encontrado = false;
        // Loop
              for (user of users) {
                    console.log("====== En el for of simple 4-1 ======");
                    console.log("Valor usuario :" + user.id);
                    if (user.id == req.params.id) {
                      encontrado = true;
                      console.log('****** localizado ******');
                      console.log('Quieres borrar el Id :' + req.params.id);
                      console.log('Y en el fichero es   :' + user.id);
                      console.log('Y su nombre es       :' + user.first_name);
                      users.splice(i, 1);
                      break;
                    }
                    i ++;
                  }

              if (encontrado) {
                 console.log("Array of lista para ser cargada: usuario borrado");
                 writeUserDatatofile(users);
                 res.send({"msg" : "Usuario borrado con For tipo-4-1"});
               }
              else{
                 console.log("Usuario no encontrado con array of");
                 res.send({"msg" : "Usuario NO borrado"})
                 }
              }
            )
//
// DELETE con FOR tipo 4-2: array on.
//
      app.delete("/apitechu/v1/users/For4-2/:id",
        function(req, res) {

          console.log ("DELETE /apitechu/v1/users/For4-2:id");
          console.log ("id es " + req.params.id);

          var users = require('./usuarios.json'); // Carga el objeto users con el fichero

          encontrado = false;
    // Loop
          for (var [i, user] of users.entries()) {
                console.log("====== En el for of 4-2 ======");
                console.log("Usuario :" + i);
                console.log("Valor usuario :" + user.id);
                if (user.id == req.params.id) {
                    encontrado = true;
                    console.log('****** localizado ******');
                    console.log('Quieres borrar el Id :' + req.params.id);
                    console.log('Y en el fichero es   :' + user.id);
                    users.splice(i, 1);
                    break;
                }
              }

          if (encontrado) {
             console.log("Array on lista para ser cargada: usuario borrado");
             writeUserDatatofile(users);
             res.send({"msg" : "Usuario borrado con For tipo-4-2"});
           }
          else{
             console.log("Usuario no encontrado con array on");
             res.send({"msg" : "Usuario NO borrado"})
             }
          }
        )

        //
        // DELETE con FOR tipo 5: findIndex.
        //
              app.delete("/apitechu/v1/users/For5/:id",
                function(req, res) {

                  console.log ("DELETE /apitechu/v1/users/For5:id");
                  console.log ("id es " + req.params.id);

                  var users = require('./usuarios.json'); // Carga el objeto users con el fichero

                  encontrado = false;
            // Loop
                  for (var indexOfElement = users.findIndex(function(element)) {
                        return element.id == req.params.id
                      }
                    
                        console.log("====== En el for of 4-5 ======");
                        console.log("Usuario :" + i);
                        console.log("Valor usuario :" + element.id);
                        if (indexOfElement > 0) {
                            encontrado = true;
                            console.log('****** localizado ******');
                            console.log('Quieres borrar el Id :' + req.params.id);
                            console.log('Y en el fichero es   :' + user.id);
                            users.splice(indexOfElement, 1);
                            break;
                        }
                      }

                  if (encontrado) {
                     console.log("Array findIndex lista para ser cargada: usuario borrado");
                     writeUserDatatofile(users);
                     res.send({"msg" : "Usuario borrado con For tipo-5"});
                   }
                  else{
                     console.log("Usuario no encontrado con array findIndex");
                     res.send({"msg" : "Usuario NO borrado"})
                     }
                  }
                )
















































//
// Practica inicial
//
app.get('/apitechu/v1/users_practica',
  function (req, res) {
    console.log("GET /apitechu/v1/users_practica <---va bien");

    var usuario ={};

    usuario.users = require('./usuarios.json');

    if (req.query.$count=="true") {
        usuario.count = usuario.users.length;
        console.log("Solicita filtro count");
        console.log(req.query.$count);
        console.log(usuario.users.length);
    }

    if (req.query.$top) {
        usuario.users = usuario.users.slice(0, req.query.$top); // se queda con los "n" requeridos. el 0 es el primero.
        console.log("Solicita filtro top");
        console.log(req.query.$top);
    }

    res.send(usuario);

}

)

//
// La solucion del profesor
//
app.get('/apitechu/v1/users_profesor',

 function (req, res) {

   console.log("GET /apitechu/v1/users_profesor");

   var result = {}; // creamos el objeto array result
   var users = require('./usuarios.json'); // creamos el objeto array users
                                           // y lo cargamos con el fichero
   var diferencia = {}; //

   if (req.query.$count == "true") {
     console.log("Count needed");
     result.count = users.length; // al objeto array result le añadimos la propiedad count
                                  // que incluye la longitud del array users.
   }
     result.users = req.query.$top ? // al objeto array result le añadimos la propiedad users
                                     // que incluye el numero de regs. que queremos extraer y enviar.
     users.slice(0, req.query.$top) : users; // dejamos en el array los registros que queremos
     result.diferencia = users.length - req.query.$top;
     console.log ('Registros totales: '  + users.length);
     console.log ('Registros pedidos: '  + req.query.$top);
     console.log ('totales menos los seleccionados: ' + result.diferencia);
     res.send(result); // envío el array result con sus dos propiedades: count y users
 }
)

// Para ver los métodos de enviar datos desde postman
//    * desde la linea de la url: parametros.
//    * desde la linea de la url: query string.
//    * desde las cabeceras: Headers.
//    * desde el body: como JSON.

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("Parametros desde server-AFE");
    console.log(req.params);

    console.log("Query string desde server-AFE");
    console.log(req.query);

    console.log("Headers desde server-AFE");
    console.log(req.headers);

    console.log("Body desde server-AFE");
    console.log(req.body);

  }
)

//=================== Funcion para escribir un array en fichero ======
//
// --->> Sacamos a una funcion aparte la grabación en fichero
//
function writeUserDatatofile(data) {
  console.log("writeUserDatatoFile");

  const fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8", // escritura efectiva en el fichero usuarios.json
    function(err){
    if (err) {
      console.log(err);
    } else {
      console.log("Usuario persistido");
    }
  }
)
}
// =======================================================================
