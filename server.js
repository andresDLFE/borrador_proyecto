require('dotenv').config();

const express = require('express');
const app = express();

const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}

app.use(express.json()); // Pasa a JSON todo lo que se deja en el body
app.use(enableCORS);


const userController = require('./controllers/UserController'); // ruta del controlador
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

app.listen(port);
console.log("API escuchando en el puerto de AFE-plus beep bepp " + port);

app.get('/apitechu/v1/hello',
  function (req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Hola desde API TechU"});
  }
)
// GET refactorizado con UserController.js
//
app.get('/apitechu/v1/users', userController.getUsersV1)
app.get('/apitechu/v2/users', userController.getUsersV2)
app.get('/apitechu/v2/users/:id', userController.getUsersByIdV2)
app.get('/apitechu/v2/account/:idUsuario', accountController.getAccountByIdV2)
//
// POST V1 y V2
//
app.post('/apitechu/v1/users',  userController.createUserV1)
app.post('/apitechu/v2/users',  userController.createUserV2)
//
// DELETE con FOR tipo 1. Normal.
app.delete("/apitechu/v1/users/For1/:id", userController.deleteUserV1)
//
// POST
app.post('/apitechu/v1/login',  authController.loginUserV1)
app.post('/apitechu/v2/login',  authController.loginUserV2)

app.post('/apitechu/v1/logout/:id',  authController.logoutUserV1)
app.post('/apitechu/v2/logout/:id',  authController.logoutUserV2)




//

/*
//
// DELETE con FOR tipo 2. Array.forEach.
//
app.delete("/apitechu/v1/users/For2/:id",
  function(req, res) {
    console.log ("DELETE /apitechu/v1/users/For2:id");
    console.log ("id es " + req.params.id);

    var users = require('./usuarios.json'); // Carga el objeto users con el fichero

    var i;
    var valorusuario;
    encontrado = false;
// Loop
    users.forEach(function(valorusuario, i) {
          console.log("====== En el array foreach =======");
          console.log("Usuario :" + valorusuario.id);
          console.log("i       :" + i);
//          if (users[i].id == req.params.id) {
          if (valorusuario.id == req.params.id) {
            encontrado = true;
            console.log('****** localizado ******');
            console.log('Quieres borrar el Id :' + users[i].id);
            console.log('Y en el fichero es   :' + req.params.id);
            console.log('Ocurrencia numero    :' + i);
            users.splice(i, 1);
          }
        }
      );

    if (encontrado) {
       console.log("ArrayforEach users lista para ser cargada: usuario borrado");
       writeUserDatatofile(users);
       res.send({"msg" : "Usuario borrado con For tipo-2"})
     }
    else{
       console.log("Usuario no encontrado con ArrayforEach");
       res.send({"msg" : "Usuario NO borrado"})
       }
    }
  )

//
// DELETE con FOR tipo 3: array in.
//
  app.delete("/apitechu/v1/users/For3/:id",
    function(req, res) {
      console.log ("DELETE /apitechu/v1/users/For3:id");
      console.log ("id es " + req.params.id);

      var users = require('./usuarios.json'); // Carga el objeto users con el fichero

      encontrado = false;
// Loop
      for (arrayId in users) {
            console.log("====== En el arrayId in array ======");
            console.log("Usuario :" + arrayId);
            console.log("Valor usuario :" + users[arrayId].id);
            if (users[arrayId].id == req.params.id) {
              encontrado = true;
              console.log('****** localizado ******');
              console.log('Quieres borrar el Id :' + users[arrayId].id);
              console.log('Y en el fichero es   :' + req.params.id);
              users.splice(arrayId, 1);
            }
          }

      if (encontrado) {
         console.log("arrayId in array users lista para ser cargada: usuario borrado");
         writeUserDatatofile(users);
         res.send({"msg" : "Usuario borrado con For tipo-3"});
       }
      else{
         console.log("Usuario no encontrado con arrayId in array");
         res.send({"msg" : "Usuario NO borrado"})
         }
      }
    )

//
// DELETE con FOR OF tipo 4-1: array on.
//
          app.delete("/apitechu/v1/users/For4-1/:id",
            function(req, res) {

              console.log ("DELETE /apitechu/v1/users/For4-1:id");
              console.log ("id es " + req.params.id);

              var users = require('./usuarios.json'); // Carga el objeto users con el fichero

              var i = 0;
              encontrado = false;
        // Loop
              for (user of users) {
                    console.log("====== En el for of simple 4-1 ======");
                    console.log("Valor usuario :" + user.id);
                    if (user.id == req.params.id) {
                      encontrado = true;
                      console.log('****** localizado ******');
                      console.log('Quieres borrar el Id :' + req.params.id);
                      console.log('Y en el fichero es   :' + user.id);
                      console.log('Y su nombre es       :' + user.first_name);
                      users.splice(i, 1);
                      break;
                    }
                    i ++;
                  }

              if (encontrado) {
                 console.log("Array of lista para ser cargada: usuario borrado");
                 writeUserDatatofile(users);
                 res.send({"msg" : "Usuario borrado con For tipo-4-1"});
               }
              else{
                 console.log("Usuario no encontrado con array of");
                 res.send({"msg" : "Usuario NO borrado"})
                 }
              }
            )
//
// DELETE con FOR tipo 4-2: array on.
//
      app.delete("/apitechu/v1/users/For4-2/:id",
        function(req, res) {

          console.log ("DELETE /apitechu/v1/users/For4-2:id");
          console.log ("id es " + req.params.id);

          var users = require('./usuarios.json'); // Carga el objeto users con el fichero

          encontrado = false;
    // Loop
          for (var [i, user] of users.entries()) {
                console.log("====== En el for of 4-2 ======");
                console.log("Usuario :" + i);
                console.log("Valor usuario :" + user.id);
                if (user.id == req.params.id) {
                    encontrado = true;
                    console.log('****** localizado ******');
                    console.log('Quieres borrar el Id :' + req.params.id);
                    console.log('Y en el fichero es   :' + user.id);
                    users.splice(i, 1);
                    break;
                }
              }

          if (encontrado) {
             console.log("Array on lista para ser cargada: usuario borrado");
             writeUserDatatofile(users);
             res.send({"msg" : "Usuario borrado con For tipo-4-2"});
           }
          else{
             console.log("Usuario no encontrado con array on");
             res.send({"msg" : "Usuario NO borrado"})
             }
          }
        )

        //
        // DELETE con FOR tipo 5: findIndex.
        //
                app.delete("/apitechu/v1/users/For5/:id",
                function(req, res) {

                  console.log ("DELETE /apitechu/v1/users/For5:id");
                  console.log ("id es " + req.params.id);

                  var users = require('./usuarios.json'); // Carga el objeto users con el fichero

                  encontrado = false;
            // Loop
                  for (var indexOfElement = users.findIndex(function(element)) {
                        return element.id == req.params.id
                      }

                        console.log("====== En el for of 4-5 ======");
                        console.log("Usuario :" + i);
                        console.log("Valor usuario :" + element.id);
                        if (indexOfElement > 0) {
                            encontrado = true;
                            console.log('****** localizado ******');
                            console.log('Quieres borrar el Id :' + req.params.id);
                            console.log('Y en el fichero es   :' + user.id);
                            users.splice(indexOfElement, 1);
                            break;
                        }
                      }

                  if (encontrado) {
                     console.log("Array findIndex lista para ser cargada: usuario borrado");
                     writeUserDatatofile(users);
                     res.send({"msg" : "Usuario borrado con For tipo-5"});
                   }
                  else{
                     console.log("Usuario no encontrado con array findIndex");
                     res.send({"msg" : "Usuario NO borrado"})
                     }
                  }
                )
*/


//
// Practica inicial
//
app.get('/apitechu/v1/users_practica',
  function (req, res) {
    console.log("GET /apitechu/v1/users_practica <---va bien");

    var usuario ={};

    usuario.users = require('./usuarios.json');

    if (req.query.$count=="true") {
        usuario.count = usuario.users.length;
        console.log("Solicita filtro count");
        console.log(req.query.$count);
        console.log(usuario.users.length);
    }

    if (req.query.$top) {
        usuario.users = usuario.users.slice(0, req.query.$top); // se queda con los "n" requeridos. el 0 es el primero.
        console.log("Solicita filtro top");
        console.log(req.query.$top);
    }

    res.send(usuario);

}

)

//
// La solucion del profesor
//
app.get('/apitechu/v1/users_profesor',

 function (req, res) {

   console.log("GET /apitechu/v1/users_profesor");

   var result = {}; // creamos el objeto array result
   var users = require('./usuarios.json'); // creamos el objeto array users
                                           // y lo cargamos con el fichero
   var diferencia = {}; //

   if (req.query.$count == "true") {
     console.log("Count needed");
     result.count = users.length; // al objeto array result le añadimos la propiedad count
                                  // que incluye la longitud del array users.
   }
     result.users = req.query.$top ? // al objeto array result le añadimos la propiedad users
                                     // que incluye el numero de regs. que queremos extraer y enviar.
     users.slice(0, req.query.$top) : users; // dejamos en el array los registros que queremos
     result.diferencia = users.length - req.query.$top;
     console.log ('Registros totales: '  + users.length);
     console.log ('Registros pedidos: '  + req.query.$top);
     console.log ('totales menos los seleccionados: ' + result.diferencia);
     res.send(result); // envío el array result con sus dos propiedades: count y users
 }
)

// Para ver los métodos de enviar datos desde postman
//    * desde la linea de la url: parametros.
//    * desde la linea de la url: query string.
//    * desde las cabeceras: Headers.
//    * desde el body: como JSON.

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("Parametros desde server-AFE");
    console.log(req.params);

    console.log("Query string desde server-AFE");
    console.log(req.query);

    console.log("Headers desde server-AFE");
    console.log(req.headers);

    console.log("Body desde server-AFE");
    console.log(req.body);

  }
)

//=================== Funcion para escribir un array en fichero ======
//
// --->> Sacamos a una funcion aparte la grabación en fichero
//
//  SE SACA A IO.JS
// =======================================================================
