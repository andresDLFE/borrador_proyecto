const fs = require('fs');

function writeUserDatatofile(data) {
  console.log("writeUserDatatoFile");

  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8", // escritura efectiva en el fichero usuarios.json
    function(err){
    if (err) {
      console.log(err);
    } else {
      console.log("Usuario persistido");
    }
  }
)
}

module.exports.writeUserDatatofile = writeUserDatatofile;
