const io = require('../io.js');
const requestJson = require('request-json');
const crypt = require('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuafe1oed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
//
// POST Login V1
//
function loginUserV1(req, res) {
  console.log("POST /apitechu/v1/login");

  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../usuarios.json');

  var result ={};

  // Loop
  encontrado = false;
  var i;
  for (i = 0; i < users.length; i++) {
      console.log('---------------------');
      console.log('En bucle (indice):            ' + i);
      console.log('longitud del fichero (hasta): ' + users.length);
      console.log('Correo body    : ' + req.body.email);
      console.log('Correo fichero : ' + users[i].email);
      console.log('password body    : ' + req.body.email);
      console.log('password fichero : ' + users[i].email);
      if (users[i].email == req.body.email && users[i].password == req.body.password) {
          encontrado = true;
          console.log('****** localizado ******');
          console.log('Quieres borrar el email    :' + users[i].email);
          console.log('Quieres borrar el password :' + users[i].password);

          result.id = users[i].id;
          users[i].logged=true;
          break; // encontrado y purgado del array.
      }
    };
  // Fin loop
  if (encontrado) {
     console.log("Encontrado");
     io.writeUserDatatofile(users);
//     res.send(result);
     res.send({"msg" : "Login correcto", "idUsuario" : users[i].id})
   }
  else{
//     console.log("No encontrado");
     res.send({"msg" : "Usuario Login incorrecto"})
     }
}
//
// POST Login V2
//
function loginUserV2(req, res) {
  console.log("POST /apitechu/v2/login");
  console.log(req.body.email);
  console.log(req.body.password);
  var query = 'q={"email": "' + req.body.email + '"}';
  console.log("La query es :" + query);
//
//Crea cliente http
//
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para GET BY ID V2");
//
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, bodyGet) {

      if (bodyGet.length > 0) {
        var response = bodyGet[0];
        console.log("req.body.password: " + req.body.password);
        console.log("bodyGet.password: " + bodyGet[0].password);
        var encontrado = crypt.checkPassword(req.body.password, bodyGet[0].password);
        console.log("Encontrado: " + encontrado);

        if (encontrado) {
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody))
          var response = {"msg" : "login correcto" , "idUsuario" : bodyGet[0].id}
        } else {
          var response = {
            "msg" : "Password incorrecta"
          }
        }
      } else {
        var response = {
          "msg" : "Usuario no encontrado"
        }
        res.status(500);
      }
      res.send(response);
    }
  )
}
//
// LOGOUT V1
//
function logoutUserV1(req, res) {
  console.log("POST /apitechu/v1/logout");

  console.log(req.params.id);

  var users = require('../usuarios.json');

//  var result ={};

  // Loop
  encontrado = false;
  var i;
  for (i = 0; i < users.length; i++) {
      if (users[i].id == req.params.id && users[i].logged == true) {
          encontrado = true;
          console.log('****** localizado y logado ******');
          console.log('Id     :' + users[i].id);
          console.log('Logado :' + users[i].logged);
//          result.id = users[i].id;
          delete users[i].logged;
          break;
      }
    };
  // Fin loop
  if (encontrado) {
     console.log("Encontrado");
     io.writeUserDatatofile(users);
//     res.send(result);
     res.send({"msg" : "Logout correcto", "idUsuario" : users[i].id})
   }
  else{
     console.log("No encontrado");
     res.send({"msg" : "Logout incorrecto"})
     }
}
//
// LOGOUT V2
//
function logoutUserV2(req, res) {
  console.log("POST /apitechu/v2/logout");
  console.log("ID:" + req.params.id);
  var query = 'q={"id": ' + req.params.id + '}';
  console.log("La query es :" + query);
//
// Crea el cliente
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para LOGOUT V2");
//
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, bodyGet) {

    if (bodyGet.length > 0) {
      console.log("req.param.id:   " + req.param.id);
      console.log("bodyGet.id:     " + bodyGet[0].id);
      console.log("bodyGet.logged: " + bodyGet[0].logged);

      if (bodyGet[0].logged) {
          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody))
          var response = {"msg" : "Des-loggado"}
          }
          else {
              var response = {"msg" : "Usuario sin loggar"}
          }
        }
    else {
      var response = {    "msg" : "Usuario no encontrado"}
      res.status(404);
    }
    res.send(response);
  }
)
}









































/*
  var users = require('../usuarios.json');

//  var result ={};

  // Loop
  encontrado = false;
  var i;
  for (i = 0; i < users.length; i++) {
      if (users[i].id == req.params.id && users[i].logged == true) {
          encontrado = true;
          console.log('****** localizado y logado ******');
          console.log('Id     :' + users[i].id);
          console.log('Logado :' + users[i].logged);
//          result.id = users[i].id;
          delete users[i].logged;
          break;
      }
    };
  // Fin loop
  if (encontrado) {
     console.log("Encontrado");
     io.writeUserDatatofile(users);
//     res.send(result);
     res.send({"msg" : "Logout correcto", "idUsuario" : users[i].id})
   }
  else{
     console.log("No encontrado");
     res.send({"msg" : "Logout incorrecto"})
     }
}
*/






module.exports.loginUserV1=loginUserV1;
module.exports.loginUserV2=loginUserV2;

module.exports.logoutUserV1=logoutUserV1;
module.exports.logoutUserV2=logoutUserV2;
