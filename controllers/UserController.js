const io = require('../io.js');
const requestJson = require('request-json');

const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuafe1oed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
//
// GET
//
function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");
  var users = require('../usuarios.json'); // dos niveles atras
  res.send(users);
}
//
// GET V2
//
function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body) {
        var response = !err ? body : { // decide ternario
          "msg" : "error obteniendo usuarios"
        }
        res.send(response);
    }
  )
}
//
// GET USER BY ID V2
//
function getUsersByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");
  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("La query es :" + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para GET BY ID V2");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
        if (err) {
            var response = {
              "msg" : "error obteniendo usuario by ID V2"
            }
            res.status(500);
        } else {
          if (body.length > 0) {
            var response = body[0];
          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
        res.send(response);
    }
  )
}
//
// POST
//
function createUserV1(req, res) {
  console.log("POST /apitechu/v1/hello");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
  }

  var users = require('../usuarios.json');
  users.push(newUser); // empotra el nuevo usuario de la cabecera en el usuarios.json
  io.writeUserDatatofile(users); // funcion externalizada al fichero io.js
  console.log("Usuario añadido con éxito");
  res.send({"msg" : "Usuario añadido con exito"})

}
//
// POST V2
//
function createUserV2(req, res) {
  console.log("POST /apitechu/v2/users");
  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
  }

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para POST BY ID V2");

  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body) {
          console.log("Usuario creado con exito en MLab");
          res.status(201).send({"msg":"Usuario guardado"});
    }
  )
}


//
// DELETE
//
function deleteUserV1(req, res) {
  console.log ("DELETE /apitechu/v1/users/For1:id");
  console.log ("id es " + req.params.id);

  var users = require('../usuarios.json'); // Carga el objeto users con el fichero
// Loop
      encontrado = false;
      var i;
      for (i = 0; i < users.length; i++) {
        console.log('---------------------');
        console.log('En bucle (indice):            ' + i);
        console.log('longitud del fichero (hasta): ' + users.length);
        console.log('Id a buscar: ' + req.params.id);
        console.log('Usuario del fichero: ' + users[i].id);
        if (users[i].id == req.params.id) {
            encontrado = true;
            console.log('****** localizado ******');
            console.log('Quieres borrar el Id :' + users[i].id);
            console.log('Y en el fichero es   :' + req.params.id);
            console.log('Ocurrencia numero    :' + i);
            // Quitamos del array el que nos interesa
            users.splice(i, 1);                // Parametro 1: ocurrencia del array nº
                                               // Parametro 2: numero de elementos a cortar
            break; // encontrado y purgado del array.
        }
      };
// Fin loop
//
// Array users listo para ser cargado sin el registro seleccionado.
  if (encontrado) {
     console.log("Array users lista para ser cargada: usuario borrado");
     io.writeUserDatatofile(users);
     res.send({"msg" : "Usuario borrado con For tipo-1"})
   }
  else{
     console.log("Usuario no encontrado");
     res.send({"msg" : "Usuario NO borrado"})
     }
}

module.exports.getUsersV1=getUsersV1;
module.exports.getUsersV2=getUsersV2;
module.exports.getUsersByIdV2=getUsersByIdV2;
module.exports.createUserV2=createUserV2;
module.exports.createUserV1=createUserV1;
module.exports.deleteUserV1=deleteUserV1;
