const io = require('../io.js');
const requestJson = require('request-json');

const crypt = require('../crypt');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuafe1oed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
//
// GET ACCOUNT BY ID V2
//
function getAccountByIdV2(req, res) {
  console.log("GET /apitechu/v2/account/:idUsuario");
  var idUsuario = req.params.idUsuario;
  var query = 'q={"idUsuario":' + idUsuario + '}';
  console.log("La query es :" + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para GET BY ID V2");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
        if (err) {
            var response = {
              "msg" : "error obteniendo cuenta by idUsuario V2"
            }
            res.status(500);
        } else {
          if (body.length > 0) {
            var response = body;
          } else {
            var response = {
              "msg" : "Usuario sin cuentas"
            }
            res.status(404);
          }
        }
        res.send(response);
    }
  )
}

module.exports.getAccountByIdV2=getAccountByIdV2;
